<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="200" alt="Nest Logo" /></a>
</p>

[circleci-image]: https://img.shields.io/circleci/build/github/nestjs/nest/master?token=abc123def456
[circleci-url]: https://circleci.com/gh/nestjs/nest

  <p align="center">A progressive <a href="http://nodejs.org" target="_blank">Node.js</a> framework for building efficient and scalable server-side applications.</p>
    <p align="center">
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/v/@nestjs/core.svg" alt="NPM Version" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/l/@nestjs/core.svg" alt="Package License" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/dm/@nestjs/common.svg" alt="NPM Downloads" /></a>
<a href="https://circleci.com/gh/nestjs/nest" target="_blank"><img src="https://img.shields.io/circleci/build/github/nestjs/nest/master" alt="CircleCI" /></a>
<a href="https://coveralls.io/github/nestjs/nest?branch=master" target="_blank"><img src="https://coveralls.io/repos/github/nestjs/nest/badge.svg?branch=master#9" alt="Coverage" /></a>
<a href="https://discord.gg/G7Qnnhy" target="_blank"><img src="https://img.shields.io/badge/discord-online-brightgreen.svg" alt="Discord"/></a>
<a href="https://opencollective.com/nest#backer" target="_blank"><img src="https://opencollective.com/nest/backers/badge.svg" alt="Backers on Open Collective" /></a>
<a href="https://opencollective.com/nest#sponsor" target="_blank"><img src="https://opencollective.com/nest/sponsors/badge.svg" alt="Sponsors on Open Collective" /></a>
  <a href="https://paypal.me/kamilmysliwiec" target="_blank"><img src="https://img.shields.io/badge/Donate-PayPal-ff3f59.svg"/></a>
    <a href="https://opencollective.com/nest#sponsor"  target="_blank"><img src="https://img.shields.io/badge/Support%20us-Open%20Collective-41B883.svg" alt="Support us"></a>
  <a href="https://twitter.com/nestframework" target="_blank"><img src="https://img.shields.io/twitter/follow/nestframework.svg?style=social&label=Follow"></a>
</p>
  <!--[![Backers on Open Collective](https://opencollective.com/nest/backers/badge.svg)](https://opencollective.com/nest#backer)
  [![Sponsors on Open Collective](https://opencollective.com/nest/sponsors/badge.svg)](https://opencollective.com/nest#sponsor)-->

## Descripción  Tarea


Crear una aplicación de autenticación de usuarios con JWT y proteger las rutas con guards
en NestJS.
Aplicación:
1. Creación del proyecto en NestJS (API Rest):
Iniciar un nuevo proyecto en NestJS para construir una API Rest en Node.js con TypeScript.
2. Implementación de controladores, servicios y repositorios:
Organizar la lógica de negocio con controladores para manejar las solicitudes HTTP,
servicios para la lógica de la aplicación y repositorios para interactuar con la base de datos.
3. Definición de entidades y DTOs (Objetos de Transferencia de Datos):
Crear entidades que representen los modelos de datos, como Usuario, Password, etc., y
definir DTOs para especificar la estructura de los datos transmitidos entre cliente y servidor.
4. Configuración de la base de datos en PostgreSQL y conexión con la
aplicación:
Configurar una base de datos en PostgreSQL para almacenar y gestionar la información de
usuarios y tokens JWT, estableciendo la conexión con la aplicación NestJS para
operaciones CRUD.
5. Implementación de middleware:
Utilizar middleware para interceptar y manipular solicitudes y respuestas HTTP.
6. Desarrollo de una estrategia de autenticación con JWT:
Crear una estrategia de autenticación basada en JWT para permitir el inicio de sesión
seguro de los usuarios. Los tokens JWT proporcionarán autenticación sin almacenar
información de sesión en el servidor.
7. Protección de las rutas con guards que extienden de la estrategia JWT:
Implementar guards para restringir el acceso a ciertas rutas de la API, validando la
autenticidad de los tokens JWT proporcionados por los usuarios. Estos guards extenderán
la estrategia JWT, garantizando que solo los usuarios autenticados accedan a recursos
protegidos.

## Instalación

```bash
$ npm install
```

## Para correr la aplicación

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```


