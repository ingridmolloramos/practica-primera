import { ConflictException, Injectable, UnauthorizedException } from '@nestjs/common';
import { CreateUsuarioDto } from './dto/create-usuario.dto';
import { UpdateUsuarioDto } from './dto/update-usuario.dto';
import { Usuario } from './entities/Usuario';
import { InjectRepository } from '@nestjs/typeorm';
import { UsuarioRepository } from './usuario.repository';


@Injectable()
export class UsuarioService {
  constructor(private readonly usuarioRepository: UsuarioRepository) {}


  async create(createUsuarioDto: CreateUsuarioDto): Promise<Usuario>{
    const usuarioExistente= await this.usuarioRepository.buscarPorNombre(
      createUsuarioDto.nombreUsuario,
    );
    if(usuarioExistente){
        throw new ConflictException('Usuario Existente');
      }
    return this.usuarioRepository.crear(createUsuarioDto);
  }

  async validarUsuario(
    nombreUsuario:string, 
    password: string,
  ): Promise<Usuario>{
    const usuarioExistente= 
    await this.usuarioRepository.buscarPorNombre(nombreUsuario);
    if (
      !usuarioExistente || 
      usuarioExistente.password !== password || 
      usuarioExistente.nombreUsuario !== nombreUsuario ){
      throw new UnauthorizedException('Nombre de Ususario o Contraseña incorrectos');
    }
      return usuarioExistente;
  }

  findOne(id: number): Promise<Usuario> {
    return this.usuarioRepository.buscarPorId(id);
  }

  findAll() {
    return this.usuarioRepository.listar();
  }

  update(id: number, updateUsuarioDto: UpdateUsuarioDto) {
    const usuario = this.usuarioRepository.buscarPorId(id);
    if (!usuario){
      throw new Error('Usuario con id ${id} no se ha encontrado');
    }
    return this.usuarioRepository.actualizar(id, updateUsuarioDto);
 
  } 

  remove(id: number) {
    return this.usuarioRepository.eliminar(id); `This action removes a #${id} usuario`;
  }
}
