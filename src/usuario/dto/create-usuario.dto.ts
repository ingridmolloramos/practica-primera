import { IsAlphanumeric, IsEmail, IsNotEmpty,  IsString, MinLength } from "class-validator";

export class CreateUsuarioDto {

    @IsString()
    @IsNotEmpty()
    @MinLength(5,{
        message: 'El nombre del usuario deberia contener al menos 5 caracteres',
    })
    nombre: string;
    
    @IsString()
    @IsNotEmpty()
    @MinLength(3,{
        message: 'El nickname del usuario deberia contener al menos 3 caracteres',
    })
    @IsAlphanumeric( null, {message: 'Solo de permiten números y letras'})
    nombreUsuario: string;

    @IsString()
    @IsNotEmpty()
    @IsEmail(null,{message:'Ingrese un email válido'})
    email: string;

    @IsString()
    @IsNotEmpty()
    password: string;
}
