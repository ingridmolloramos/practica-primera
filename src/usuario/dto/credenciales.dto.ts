import { IsEmail, IsNotEmpty, IsString, MinLength } from "class-validator";

export class credencialesDTO{
    @IsString()
    @IsNotEmpty()
    nombreUsuario:string;

    @IsString()
    @IsNotEmpty()
    @MinLength(6,{
        message: 'El password del usuario deberia contener al menos 6 caracteres',
    })
    password: string;

}