import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsuarioModule } from 'src/usuario/usuario.module';
import { Usuario } from 'src/usuario/entities/Usuario';
import { UsuarioService } from 'src/usuario/usuario.service';
import { UsuarioRepository } from 'src/usuario/usuario.repository';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from 'src/constants/constant';
import { JwtStrategy } from './jwt.strategy';

@Module({
  imports:[
    TypeOrmModule.forFeature([Usuario]), 
    UsuarioModule, 
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '360s'}
    })],
  providers: [AuthService, UsuarioService, UsuarioRepository, JwtStrategy],
  controllers: [AuthController ]
})
export class AuthModule {}
