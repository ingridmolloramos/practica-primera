import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Usuario } from 'src/usuario/entities/Usuario';

@Injectable()
export class AuthService {
    constructor(private jwtService: JwtService){}
    async login (usuario: Usuario){
        const payload = { nonbreUsuario: usuario.nombreUsuario, sub: usuario.id};
        return {
            access_token: this.jwtService.sign(payload),
        };
    }
}
